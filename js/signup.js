
$(document).ready(function () {
     $('#error-message').html('<br/>');
    setTimeout(function () {
        $('#logo-wrapper').removeClass('hidden-lg');
    }, 100);
    $('#loginWindow').animate({'width': '100%'}, 450)
        .delay(30)
        .animate({'height': '500px'}, 450);
    $('.page-header, .input-group, .btn')
        .delay(850)
        .animate({'opacity': '100'}, 7000);

    $("#submit-button").on('click', function () {
        resetFeedback();
        var email = $('#email').val();
        var pass = $('#password').val();
        var cpass = $('#Confirmpassword').val();
        var company = $('#company').val();
        if (!validateUsername(email)) {
            feedbackInvalidEmail();
            
        }

        if (!validateCompany(company)) {
            feedbackCompany();
            
        }
         else if (!validateUsername(email)) {
            feedbackInvalidEmail();

        }

       else if (!validatePassConfirm(pass,cpass)) {
            feedbackInvalidCPassword();
            
        }
        else {
            var password = $('#password').val();
        var company = $('#company').val();
            var userData = {
                id: email,
                password: password,
                company:company
            };
            $.ajax({
                url: config.baseUrl + '/signup',
                    type: 'POST',
                    dataType: 'json',
                    crossDomain: true,
                    data: JSON.stringify(userData),
                    success: function (result) {
                        if (result.status === 200) {
                            $('#error-message').html(result.message);
                        } else {
                        }
                    },
                    error: function () {}

                }
            );
        }


    });
});

function validateUsername(email) {
    var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(email);
}


function feedbackInvalidEmail() {
    $('#u16-login-email-div').addClass('has-error');
    $('#u16-login-password-div').addClass('has-error');
    $('#email').val('');
    $('#password').val('');
    $('#error-message').html('Incorrect email or password');
    $('#loginWindow').animate({'height': '500px'}, 100);
}

function feedbackInvalidPassword() {
    $('#u16-login-password-div').addClass('has-error');
    $('#password').val('');
    $('#error-message').html('Please enter valid password');
    $('#loginWindow').animate({'height': '500px'}, 100);
}

function resetFeedback() {
    $('#u16-login-password-div').removeClass('has-error');
    $('#u16-login-email-div').removeClass('has-error');
    $('#u16-login-confirm_password-div').removeClass('has-error');
    $('#u16-login-company-div').removeClass('has-error');
    $('#error-message').html('').removeClass('has-error');
    $('#loginWindow').animate({'height': '500px'}, 100);
}

function validateCompany(company){
    return company!='';
    
}

function validatePassConfirm(pass,cpass){
    return pass===cpass;
}

function feedbackInvalidCPassword() {
    $('#u16-login-password-div').addClass('has-error');
    $('#u16-login-confirm_password-div').addClass('has-error');
    $('#password').val('');
    $('#Confirmpassword').val('');
    $('#error-message').html('Passwords do not match');
    $('#loginWindow').animate({'height': '500px'}, 100);
}

function feedbackCompany(){
     $('#u16-login-company-div').addClass('has-error');
     $('#u16-login-confirm_company-div').addClass('has-error');
     $('#company').val('');
     $('#error-message').html('Company is required');
     $('#loginWindow').animate({'height': '500px'}, 100);
}