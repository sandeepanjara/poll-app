$('#my-table').on('click', 'tr td:first-child', function () {
    $("#display-graph").remove();
    $("#display-graph-2").remove();
    $('.col-sm-4').append('<canvas id="display-graph" width="100%" style="padding: 1px;height: 80%"></canvas>');
    $('.col-sm-4').append('<canvas id="display-graph-2" width="100%" style="padding: 1px;height: 80%"></canvas>');

    var myBarChart;
    var question;
    var selected = $(this).text();

        for (i = 0; i < app.question.length; i++) {
            if (app.question[i]["question"] == selected) {
                question = app.question[i];
                break;
            }
        }
        var ctx = document.getElementById("display-graph").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',


            data: {
                labels: question["options"],
                datasets: [{
                    label: question["question"],
                    data: question["answer"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(198, 206, 86, 0.2)',
                        'rgba(10, 5, 98, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(30, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(198, 206, 86, 0.2)',
                        'rgba(10, 5, 98, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(30, 102, 255, 0.2)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                title: {
            display: true,
            text: question["question"]
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        },
                legend:{
                    display:false
                }
            }

        });


        var ct = document.getElementById("display-graph-2").getContext('2d');
        var myChar = new Chart(ct, {
            type: 'pie',


            data: {
                labels: question["options"],
                datasets: [{
                    label: question["question"],
                    data: question["answer"],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(198, 206, 86, 0.2)',
                        'rgba(10, 5, 98, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(30, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(198, 206, 86, 0.2)',
                        'rgba(10, 5, 98, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(30, 102, 255, 0.2)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                animation:{
                    animateRotate:true,
                    animateScale:true
                }

    }


        });




});


function update(id) {
    userData = {
        "qid": id,
        "user": localStorage.getItem("name")
    };
    $.ajax({
            url: config.baseUrl + '/question',
            type: 'PUT',
            dataType: 'json',
            crossDomain: true,
            headers: {'Authorization': app.token},
            data: JSON.stringify(userData),
            success: function (result) {
                if (result.status === 200) {
                    reloadDatas();
                    window.location.reload();
                } else {
                    feedbackInvalidEmail();
                }
            },
            error: function () {
                console.log('Login error');
                // localStorage.removeItem('authorization-token');
                // window.location.reload();
            }
        }
    );
}