
var app = {};
 app.token = localStorage.getItem('authorization-token');

var new_path = config.baseUrl + '/file/' + 'PollPlugin_' + localStorage.getItem("name") + '.js';
$("a[href= 'http://localhost:8000/file/PollPlugin.js']").attr('href', new_path);
    $('#user').text = localStorage.getItem("name");
    $('#user').innerHTML = localStorage.getItem("name");
    reloadDatas();
    var source = $('#question-template').html();
    app.template = Handlebars.compile(source);
    if(app.question ){
        var questionTable = $('#question-table');
        var questionData = {question: app.question};

    for (var i = 0; i < questionData.question.length; i++) {
        questionData.question[i].index = i;
    }

    questionTable.html(app.template(questionData));
}
 function logOut() {
     localStorage.removeItem('authorization-token');
     localStorage.removeItem('name');
     window.location.replace('index.html');
 }

function reloadDatas() {
    name = localStorage.getItem("name");
    $.ajax({
            url: config.baseUrl + '/question' + '?' + 'type=admin' +"&" + "user=" + name,
            type: 'GET',
        dataType: 'json',
            crossDomain: true,
            headers: {'Authorization': app.token},
            success: function (result) {
                if (result.status === 200) {
                    var questions = result.message;

                    for (var i = 0; i < questions.length; i++) {
                        questions[i].question = questions[i].question.replace(/\n/g, '<br />');
                    }

                    app.question = questions;

                    var source = $('#question-template').html();
                    var template = Handlebars.compile(source);

                    var questionTable = $('#question-table');
                    questions = {question: app.question};

                    for (var i = 0; i < questions.question.length; i++) {
                        questions.question[i].index = i;
                    }

                    questionTable.html(template(questions));
                } else {
                }
            },
            error: function () {
            }
        }
    );
}

function confirmAndAddParticipant() {
    var names = $('#participant-names-add-team-modal').val().trim().split(",");
    console.log(names.length)
    var mobileNumber = $('#mobile-number-add-team-modal').val().trim();
    var name = localStorage.getItem("name");

    if (names.length > 1 && mobileNumber != "") {
        $('#add-team-modal').modal('hide');

        $.ajax({
            url: config.baseUrl + '/question',
            type: 'POST',
            data: JSON.stringify({
                question: mobileNumber,
                options: names,
                user: name

            }),
            dataType: 'json',
            headers: {'Authorization': app.token},
            crossDomain: true,
            success: function (result) {
                if (result.status === 200) {
                    window.location.reload();
                } else {
                    window.alert('Some error occurred.\nPlease try again.');
                    window.location.reload();
                }
            },
            error: function () {
                    reloadData();
                }
            }
        );
    } else {
        var alertBox = $('#alert-add-team-modal');
        var message = '';
        if (mobileNumber == "") {
            message = 'Enter valid question';
        } else {
            message = 'Enter at least two options ';
        }
        alertBox.html(message);
        alertBox.removeClass('sr-only');
    }
}