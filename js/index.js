
var token = localStorage.getItem('authorization-token');
if (token) {
    var name = localStorage.getItem("name");
    $.ajax({
        url: config.baseUrl + '/question' + '?'+ "user=" + name,
            type: 'GET',
            crossDomain: true,
            async: false,
            headers: {'Authorization': token},
            dataType: 'json',
        success: function (result) {
                if (result.status === 200) {
                    window.location.replace('dashboard.html');
                } else {
                    localStorage.removeItem('authorization-token');
                    window.location.reload();
                }
            },
            error: function () {
                localStorage.removeItem('authorization-token');
                window.location.reload();
            }
        }
    );
}

$(document).ready(function () {
     $('#error-message').html('<br/>');
    setTimeout(function () {
        $('#logo-wrapper').removeClass('hidden-lg');
    }, 100);
    $('#loginWindow').animate({'width': '100%'}, 500)
        .delay(30)
        .animate({'height': '400px'}, 500);
    $('.page-header, .input-group, .btn')
        .delay(850)
        .animate({'opacity': '100'}, 7000);

    $("#submit-button").on('click', function () {
        resetFeedback();
        var email = $('#email').val();
        if (!validateUsername(email)) {
            feedbackInvalidEmail();
        }
        var password = $('#password').val();
            var userData = {
                id: email,
                password: password
            };
            $.ajax({
                url: config.baseUrl + '/login',
                    type: 'POST',
                    dataType: 'json',
                    crossDomain: true,
                    data: JSON.stringify(userData),
                    success: function (result) {
                        if (result.status === 200) {
                            var token = result.message.token;
                            var name = result.message.name;
                            localStorage.setItem('authorization-token', token);
                            localStorage.setItem('name', name);
                            window.location.href = 'dashboard.html';
                        } else {
                            feedbackInvalidEmail();
                        }
                    },
                    error: function () {

                         localStorage.removeItem('authorization-token');
                         window.location.reload();
                    }
                }
            );

    });
});


function validateUsername(email) {
    var emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(email);
}


function feedbackInvalidEmail() {
    $('#u16-login-email-div').addClass('has-error');
    $('#u16-login-password-div').addClass('has-error');
    $('#email').val('');
    $('#password').val('');
    $('#error-message').html('Incorrect email or password');
    $('#loginWindow').animate({'height': '400px'}, 100);
}

function feedbackInvalidPassword() {
    $('#u16-login-password-div').addClass('has-error');
    $('#password').val('');
    $('#error-message').html('Please enter valid password');
    $('#loginWindow').animate({'height': '400px'}, 100);
}

function resetFeedback() {
    $('#u16-login-password-div').removeClass('has-error');
    $('#u16-login-email-div').removeClass('has-error');
    $('#error-message').html('').removeClass('has-error');
    $('#loginWindow').animate({'height': '400px'}, 100);
}